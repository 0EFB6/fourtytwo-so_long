/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/21 10:54:12 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/24 15:50:16 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "so_long.h"

int	main(void)
{
	void	*mlx_ptr;
	void	*win_ptr;
	void	*floor;
	int		width;
	int		height;

	width = 50;
	height = 50;
	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, 500, 500, "Hola World!");
	floor = mlx_xpm_file_to_image(mlx_ptr, "./sprites/pacdot_powerup.xpm",
			&width, &height);
	mlx_put_image_to_window(mlx_ptr, win_ptr, floor, 6 * PIXEL_WIDTH, 6
		* PIXEL_HEIGHT);
	mlx_loop(mlx_ptr);
}
