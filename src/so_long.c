/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 12:48:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/29 10:23:51 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	main(int ac, char **av)
{
	t_data			d;
	t_player		player;
	t_enemy			enemy;

	if (ac == 2)
	{
		parse(&d, av);
		d.player = &player;
		d.enemy = &enemy;
		init(&d);
		if (IS_LINUX)
			mlx_key_hook(d.win_ptr, handle_keycode, &d);
		else
			mlx_hook(d.win_ptr, KEYPRESS, 0, handle_keycode, &d);
		mlx_hook(d.win_ptr, DESTROY, 0, handle_destroy, &d);
		mlx_loop_hook(d.mlx_ptr, animation, &d);
		mlx_loop(d.mlx_ptr);
		return (0);
	}
	else
	{
		ft_putstr_fd("[ERROR] Enter only one parameter\n", 2);
		return (1);
	}
}
