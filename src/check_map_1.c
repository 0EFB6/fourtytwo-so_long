/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/24 17:08:11 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 22:34:24 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static int	check_char(t_data *d);
static int	check_exit(t_data *d);
static void	exit_error(t_data *d, char *message);

void	check_map(t_data *d)
{
	if (!check_exit(d))
		exit_error(d, "[ERROR] Only ONE exit (E) is needed in map\n");
	if (!check_player(d))
		exit_error(d, "[ERROR] Only ONE player (P) is needed in map\n");
	if (!check_collectibles(d))
		exit_error(d, "[ERROR] No collectibles (C) in map\n");
	if (!check_valid_path(d))
		exit_error(d, "[ERROR] No valid path in map\n");
	if (!check_min_row_column(d))
		exit_error(d, "[ERROR] Map is too small, need at least 5 rows and 5 \
columns\n");
	if (!check_rectangle(d))
		exit_error(d, "[ERROR] Map is not a rectangle\n");
	if (!check_char(d))
		exit_error(d, "[ERROR] Map contains invalid characters, use only \
0,1,C,E,P\n");
	if (!check_walls(d))
		exit_error(d, "[ERROR] Map is not surrounded by walls\n");
}

int	check_collectibles(t_data *d)
{
	int	i;
	int	j;

	i = 0;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i][j])
			if (d->map[i][j++] == 'C')
				return (1);
		i++;
	}
	return (0);
}

static int	check_char(t_data *d)
{
	int	i;
	int	j;

	i = 0;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i][j])
		{
			if (!ft_strchr(MAP_SYMBOL, d->map[i][j]))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

static int	check_exit(t_data *d)
{
	int	i;
	int	j;
	int	count;

	i = 0;
	count = 0;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i][j])
		{
			if (d->map[i][j] == 'E' && count == 0)
				count++;
			else if (d->map[i][j] == 'E' && count == 1)
				return (0);
			j++;
		}
		i++;
	}
	if (count == 0)
		return (0);
	return (1);
}

static void	exit_error(t_data *d, char *message)
{
	ft_putstr_fd(message, 2);
	handle_free_map(d);
	exit(1);
}
