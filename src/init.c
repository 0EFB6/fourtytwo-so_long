/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 13:21:32 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/29 10:04:32 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	init_image(t_data *d)
{
	int	width;
	int	height;

	d->floor = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/floor.xpm", &width, &height);
	d->exit = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/portal.xpm", &width, &height);
	d->wall = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/wall.xpm", &width, &height);
	d->black = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/wall_black.xpm", &width, &height);
	d->collect = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/collectibles_big.xpm", &width, &height);
	d->logo = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/pacman-logo.xpm", &width, &height);
	d->player->u = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/pac_open_u.xpm", &width, &height);
	d->player->d = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/pac_open_d.xpm", &width, &height);
	d->player->l = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/pac_open_l.xpm", &width, &height);
	d->player->r = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/pac_open_r.xpm", &width, &height);
}

static void	init_image_enemy(t_data *d)
{
	int	i;
	int	width;
	int	height;

	i = 0;
	d->enemy->l = malloc(sizeof(void *) * 3);
	while (i < 3)
		d->enemy->l[i++] = NULL;
	i = 0;
	d->enemy->r = malloc(sizeof(void *) * 3);
	while (i < 3)
		d->enemy->r[i++] = NULL;
	d->enemy->l[0] = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/enemy/l_1.xpm", &width, &height);
	d->enemy->l[1] = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/enemy/l_2.xpm", &width, &height);
	d->enemy->r[0] = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/enemy/r_1.xpm", &width, &height);
	d->enemy->r[1] = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/enemy/r_2.xpm", &width, &height);
	d->enemy->panic = mlx_xpm_file_to_image(d->mlx_ptr,
			"./sprites/panic_4.xpm", &width, &height);
}

static void	init_score_2(t_data *d)
{
	int	width;
	int	height;

	d->score[7] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/7.xpm",
			&width, &height);
	d->score[8] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/8.xpm",
			&width, &height);
	d->score[9] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/9.xpm",
			&width, &height);
}

static void	init_score(t_data *d)
{
	int	i;
	int	width;
	int	height;

	i = 0;
	d->score = malloc(sizeof(void *) * 11);
	while (i < 11)
		d->score[i++] = NULL;
	d->score[0] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/0.xpm",
			&width, &height);
	d->score[1] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/1.xpm",
			&width, &height);
	d->score[2] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/2.xpm",
			&width, &height);
	d->score[3] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/3.xpm",
			&width, &height);
	d->score[4] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/4.xpm",
			&width, &height);
	d->score[5] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/5.xpm",
			&width, &height);
	d->score[6] = mlx_xpm_file_to_image(d->mlx_ptr, "./sprites/6.xpm",
			&width, &height);
	init_score_2(d);
}

void	init(t_data *d)
{
	d->mlx_ptr = mlx_init();
	if (!d->mlx_ptr)
	{
		perror("Fail to initialize mlx_ptr");
		exit(1);
	}
	d->win_ptr = mlx_new_window(d->mlx_ptr, get_column(d) * PIXEL_WIDTH,
			(get_row(d) + 1) * PIXEL_HEIGHT, "Wilson's so_long");
	if (!d->win_ptr)
	{
		perror("Fail to initialize window win_ptr");
		exit(1);
	}
	d->count = 0;
	d->trigger = 0;
	d->exit_code = 0;
	d->enemy->frame = 0;
	d->enemy->i_image = 0;
	get_enemy_position(d);
	init_image(d);
	init_score(d);
	init_image_enemy(d);
	draw(d);
}
