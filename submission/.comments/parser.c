/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 18:19:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 23:42:35 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

/*
** Prompt error when invalid file descriptor
*/
static void	check_fd(int fd)
{
	if (fd == -1)
	{
		perror("Map not found");
		exit(1);
	}
}

/*
** Check file extension, then automatically join the argument with file dir
** path
** Open file and check if valid fd
** Use get_next_line() to get content of file line by line and join
** together while being assigned to the file variable
** use ft_split() to split by newline into map array
** Free all necessary variable and perform a final check of the map array
*/
void	parse(t_data *d, char **av)
{
	char	*path;
	int		fd;
	char	*line;
	char	*file;

	check_file_extension(av);
	path = ft_strjoin("./maps/", av[1]);
	fd = open(path, O_RDONLY);
	free(path);
	check_fd(fd);
	line = get_next_line(fd);
	file = ft_calloc(1, sizeof(char));
	while (line && file)
	{
		file = ft_strjoin(file, line);
		free(line);
		line = get_next_line(fd);
	}
	free(line);
	d->map = ft_split(file, '\n');
	free(file);
	check_map(d);
}
