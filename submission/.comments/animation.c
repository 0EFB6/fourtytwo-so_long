/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   animation.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/24 15:50:59 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/29 10:27:52 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	move_enemy(t_data *d);

/*
** Make the enemy electrified for a moment using the panic image when triggered
** Move the enemy to the next coordinate
*/
static void	animation_extention(t_data *d)
{
	d->trigger++;
	d->enemy->i_image++;
	if (d->enemy->i_image == 2)
	{
		if (d->trigger >= 6)
		{
			draw_image(d, d->enemy->panic, d->enemy->x, d->enemy->y);
			d->trigger = 0;
		}
		move_enemy(d);
		d->enemy->i_image = 0;
	}
}

/*
** Animate the enemy according to the frame number
** If player is at the same coordinate as the enemy, player loses
** Draw the previous coordinate if the enemy is moving to the next
** coordinate according to the indexx number
** Draw the enemy at the new coordinate with the relevant imaages
** depending on the moving direction (sign)
*/
int	animation(t_data *d)
{
	d->enemy->frame++;
	if (d->enemy->frame == 2000 && d->enemy->x < 100 && d->enemy->y < 100)
	{
		if (d->enemy->frame == 2000)
			d->enemy->frame = 0;
		if (d->enemy->x == d->player->x && d->enemy->y == d->player->y)
		{
			ft_putstr_fd("You Lose!\n", 1);
			handle_quit(d);
		}
		if (d->enemy->i_image == 0 && d->enemy->sign == 1
			&& d->map[d->enemy->y][d->enemy->x - 1] == '0')
			draw_image(d, d->black, d->enemy->x - 1, d->enemy->y);
		if (d->enemy->i_image == 0 && d->enemy->sign == 0
			&& d->map[d->enemy->y][d->enemy->x + 1] == '0')
			draw_image(d, d->black, d->enemy->x + 1, d->enemy->y);
		if (d->enemy->sign == 0)
			draw_image(d, d->enemy->l[d->enemy->i_image],
				d->enemy->x, d->enemy->y);
		else if (d->enemy->sign == 1)
			draw_image(d, d->enemy->r[d->enemy->i_image],
				d->enemy->x, d->enemy->y);
		animation_extention(d);
	}
	return (0);
}

/*
** Move the enemy right or left depending on the sign
*/
static void	move_enemy(t_data *d)
{
	if (d->enemy->sign == 1)
	{
		if (d->map[d->enemy->y][d->enemy->x + 1] == '0')
			d->enemy->x++;
		else
		{
			d->enemy->x--;
			d->enemy->sign = 0;
		}
	}
	else
	{
		if (d->map[d->enemy->y][d->enemy->x - 1] == '0')
			d->enemy->x--;
		else
		{
			d->enemy->x++;
			d->enemy->sign = 1;
		}
	}
}
