/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   score.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/26 18:09:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/29 10:37:07 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	draw_score(t_data *d, int n, int row, int column);

/*
** Draw 0 score for all 4 digits when window is initialized
*/
void	score_init(t_data *d)
{
	int	row;
	int	column;

	row = get_row(d) * PIXEL_HEIGHT + 7;
	if (get_column(d) % 2 != 0)
		column = (get_column(d) - 1) / 2;
	else
		column = get_column(d) / 2;
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->score[0],
		(column - 1) * PIXEL_WIDTH, row);
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->score[0],
		column * PIXEL_WIDTH, row);
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->score[0],
		(column + 1) * PIXEL_WIDTH, row);
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->score[0],
		(column + 2) * PIXEL_WIDTH, row);
}

/*
** Update ones, tenths, hundreds, thousands digit accordingly to the count
** variable
*/
void	score(t_data *d, int row, int column)
{
	int	n;

	n = d->count;
	if (d->count >= 10)
		while (n > 9)
			n = n % 10;
	draw_score(d, n, row, column + 2);
	if (d->count > 9)
	{
		n = ((d->count % 100) - ((d->count % 100) % 10)) / 10;
		draw_score(d, n, row, column + 1);
	}
	if (d->count > 99)
	{
		n = (d->count / 100) % 10;
		draw_score(d, n, row, column);
	}
	if (d->count > 999)
	{
		n = (d->count / 1000) % 10;
		draw_score(d, n, row, column - 1);
	}
}

static void	draw_score(t_data *d, int n, int row, int column)
{
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->black,
		column * PIXEL_WIDTH, row);
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, d->score[n],
		column * PIXEL_WIDTH, row);
}
