/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/24 17:09:04 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 23:39:41 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	check_min_row_column(t_data *d)
{
	int	row;
	int	column;

	row = get_row(d);
	column = get_column(d);
	if (row < 5 || column < 5)
		return (0);
	return (1);
}

int	check_player(t_data *d)
{
	int	i;
	int	j;
	int	player;

	i = 0;
	player = 0;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i][j])
		{
			if (d->map[i][j] == 'P' && player == 0)
				player++;
			else if (d->map[i][j] == 'P' && player == 1)
				return (0);
			j++;
		}
		i++;
	}
	if (player == 0)
		return (0);
	return (1);
}

int	check_rectangle(t_data *d)
{
	int	i;
	int	j;
	int	column;

	i = 0;
	column = 0;
	while (d->map[0] && d->map[0][column])
		column++;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i][j])
			j++;
		if (j != column)
			return (0);
		i++;
	}
	if (i == j)
		return (0);
	return (1);
}

void	check_file_extension(char **av)
{
	char	*extension;

	extension = ft_strrchr(av[1], '.');
	if (!extension || ft_strncmp(extension, ".ber", 4))
	{
		ft_putstr_fd("[ERROR] Enter a map with .ber extension\n", 2);
		exit(1);
	}
}

int	check_walls(t_data *d)
{
	int	i;
	int	row;
	int	column;

	i = 0;
	row = get_row(d);
	column = get_column(d);
	while (d->map[0][i])
		if (d->map[0][i++] != '1')
			return (0);
	i = 0;
	while (d->map[row - 1][i])
		if (d->map[row - 1][i++] != '1')
			return (0);
	i = 0;
	while (d->map[i])
	{
		if (d->map[i][0] != '1' || d->map[i][column - 1] != '1')
			return (0);
		i++;
	}
	return (1);
}
