/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 13:02:24 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 23:52:38 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	handle_free_map(t_data *d)
{
	int	i;

	i = 0;
	while (d->map[i])
		free(d->map[i++]);
	free(d->map);
}

int	handle_keycode(int keycode, t_data *data)
{
	if ((keycode == 13 || keycode == 126)
		|| (IS_LINUX && (keycode == 65362 || keycode == 119)))
		u(data);
	else if ((keycode == 1 || keycode == 125)
		|| (IS_LINUX && (keycode == 65364 || keycode == 115)))
		d(data);
	else if ((keycode == 0 || keycode == 123)
		|| (IS_LINUX && (keycode == 65361 || keycode == 97)))
		l(data);
	else if ((keycode == 2 || keycode == 124)
		|| (IS_LINUX && (keycode == 65363 || keycode == 100)))
		r(data);
	else if (keycode == 53 || (IS_LINUX && keycode == 65307))
		handle_quit(data);
	return (0);
}

int	handle_destroy(t_data *d)
{
	handle_free_map(d);
	free(d->enemy->l);
	free(d->enemy->r);
	free(d->enemy->panic);
	free(d->player->u);
	free(d->player->d);
	free(d->player->l);
	free(d->player->r);
	free(d->exit);
	free(d->wall);
	free(d->logo);
	free(d->black);
	free(d->floor);
	free(d->collect);
	free(d->score);
	exit(0);
	return (0);
}

void	handle_quit(t_data *d)
{
	mlx_destroy_window(d->mlx_ptr, d->win_ptr);
	handle_free_map(d);
	free(d->enemy->l);
	free(d->enemy->r);
	free(d->enemy->panic);
	free(d->player->u);
	free(d->player->d);
	free(d->player->l);
	free(d->player->r);
	free(d->exit);
	free(d->wall);
	free(d->logo);
	free(d->black);
	free(d->floor);
	free(d->collect);
	free(d->score);
	exit(0);
}
