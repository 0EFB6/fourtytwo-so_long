/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 10:06:34 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/27 00:10:23 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H
# include <fcntl.h> // open
# include <stdlib.h> // size_t
# include <stdio.h> // printf
# include <unistd.h> // write
# include "mlx.h"
# include "libft.h"

# ifdef __linux__
#  define IS_LINUX 1
# else
#  define IS_LINUX 0
# endif
# define KEYPRESS 2
# define DESTROY 17
# define PIXEL_WIDTH 32
# define PIXEL_HEIGHT 32
# define MAP_SYMBOL "01CEP"

typedef struct s_player
{
	int		x;
	int		y;
	void	*u;
	void	*d;
	void	*l;
	void	*r;
}	t_player;

typedef struct s_enemy
{
	int		x;
	int		y;
	int		sign;
	int		frame;
	int		i_image;
	void	**l;
	void	**r;
	void	*panic;
}	t_enemy;

typedef struct s_data
{
	int			count;
	int			trigger;
	int			exit_code;
	char		**map;
	void		*exit;
	void		*wall;
	void		*logo;
	void		*black;
	void		*floor;
	void		*collect;
	void		*mlx_ptr;
	void		*win_ptr;
	void		**score;
	t_player	*player;
	t_enemy		*enemy;
}	t_data;

/* animation.c */
int		animation(t_data *d);

/* check_map_1.c */
void	check_map(t_data *d);
int		check_collectibles(t_data *d);

/* check_map_2.c */
int		check_min_row_column(t_data *d);
int		check_player(t_data *d);
int		check_rectangle(t_data *d);
void	check_file_extension(char **av);
int		check_walls(t_data *d);

/* check_map_3.c */
int		check_valid_path(t_data *d);

/* draw.c */
void	draw_image(t_data *d, void *image, int j, int i);
void	draw_moves(t_data *d);
void	draw(t_data *d);

/* get.c */
int		get_row(t_data *d);
int		get_column(t_data *d);
void	get_enemy_position(t_data *d);

/* handler.c */
void	handle_free_map(t_data *d);
int		handle_keycode(int keycode, t_data *d);
int		handle_destroy(t_data *d);
void	handle_quit(t_data *d);

/* init.c */
void	init(t_data *d);

/* movement.c */
void	u(t_data *d);
void	d(t_data *d);
void	l(t_data *d);
void	r(t_data *d);

/* parser.c */
void	parse(t_data *d, char **av);

/* score.c */
void	score_init(t_data *d);
void	score(t_data *d, int row, int column);

#endif