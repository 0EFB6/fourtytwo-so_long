/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 18:19:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/29 10:31:48 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	check_fd(int fd)
{
	if (fd == -1)
	{
		perror("[ERROR] Map not found");
		exit(1);
	}
}

void	parse(t_data *d, char **av)
{
	char	*path;
	int		fd;
	char	*line;
	char	*file;
	char	*tmp;

	check_file_extension(av);
	path = ft_strjoin("./maps/", av[1]);
	fd = open(path, O_RDONLY);
	free(path);
	check_fd(fd);
	line = get_next_line(fd);
	file = ft_calloc(1, sizeof(char));
	while (line && file)
	{
		tmp = file;
		file = ft_strjoin(file, line);
		free(line);
		free(tmp);
		line = get_next_line(fd);
	}
	free(line);
	d->map = ft_split(file, '\n');
	free(file);
	check_map(d);
}
