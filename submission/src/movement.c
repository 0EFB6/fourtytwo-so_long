/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movement.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/23 09:50:33 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 23:14:50 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	c(t_data *d, int x, int y)
{
	d->map[y][x] = '0';
	if (!check_collectibles(d))
		d->exit_code = 1;
}

void	u(t_data *d)
{
	int	x;
	int	y;

	x = d->player->x;
	y = d->player->y;
	if (d->map[y - 1][x] != '1' && d->map[y - 1][x] != 'E')
	{
		if (x == d->enemy->x && y - 1 == d->enemy->y)
		{
			ft_putstr_fd("You Lose!\n", 1);
			handle_quit(d);
		}
		draw_image(d, d->black, x, y);
		d->player->y--;
		draw_image(d, d->floor, x, y - 1);
		draw_image(d, d->player->u, x, y - 1);
		d->count++;
		draw_moves(d);
		if (d->map[y - 1][x] == 'C')
			c(d, x, y - 1);
	}
	else if (d->map[y - 1][x] == 'E' && d->exit_code)
		handle_quit(d);
}

void	d(t_data *d)
{
	int	x;
	int	y;

	x = d->player->x;
	y = d->player->y;
	if (d->map[y + 1][x] != '1' && d->map[y + 1][x] != 'E')
	{
		if (x == d->enemy->x && y + 1 == d->enemy->y)
		{
			ft_putstr_fd("You Lose!\n", 1);
			handle_quit(d);
		}
		draw_image(d, d->black, x, y);
		d->player->y++;
		draw_image(d, d->floor, x, y + 1);
		draw_image(d, d->player->d, x, y + 1);
		d->count++;
		draw_moves(d);
		if (d->map[y + 1][x] == 'C')
			c(d, x, y + 1);
	}
	else if (d->map[y + 1][x] == 'E' && d->exit_code)
		handle_quit(d);
}

void	l(t_data *d)
{
	int	x;
	int	y;

	x = d->player->x;
	y = d->player->y;
	if (d->map[y][x - 1] != '1' && d->map[y][x - 1] != 'E')
	{
		if (x - 1 == d->enemy->x && y == d->enemy->y)
		{
			ft_putstr_fd("You Lose!\n", 1);
			handle_quit(d);
		}
		draw_image(d, d->black, x, y);
		d->player->x--;
		draw_image(d, d->floor, x - 1, y);
		draw_image(d, d->player->l, x - 1, y);
		d->count++;
		draw_moves(d);
		if (d->map[y][x - 1] == 'C')
			c(d, x - 1, y);
	}
	else if (d->map[y][x - 1] == 'E' && d->exit_code)
		handle_quit(d);
}

void	r(t_data *d)
{
	int	x;
	int	y;

	x = d->player->x;
	y = d->player->y;
	if (d->map[y][x + 1] != '1' && d->map[y][x + 1] != 'E')
	{
		if (x + 1 == d->enemy->x && y == d->enemy->y)
		{
			ft_putstr_fd("You Lose!\n", 1);
			handle_quit(d);
		}
		draw_image(d, d->black, x, y);
		d->player->x++;
		draw_image(d, d->floor, x + 1, y);
		draw_image(d, d->player->r, x + 1, y);
		d->count++;
		draw_moves(d);
		if (d->map[y][x + 1] == 'C')
			c(d, x + 1, y);
	}
	else if (d->map[y][x + 1] == 'E' && d->exit_code)
		handle_quit(d);
}
