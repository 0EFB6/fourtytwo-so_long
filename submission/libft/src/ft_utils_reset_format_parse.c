/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_reset_format_parse.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:07:53 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 00:41:23 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

// Reset the user defined structure to 0 for all values inside it to ensure that
// the values are default.
void	ft_reset(t_format *f)
{
	f->minus = 0;
	f->plus = 0;
	f->space = 0;
	f->zero = 0;
	f->hash = 0;
	f->dot = 0;
	f->width = 0;
	f->precision = 0;
	f->specifier = 0;
}

// Set the format string based on the specifier '*'
// If the character is not '*', return 0
// If the dot flag is true, set the precision to star where star is the value
// returned from va_arg
// Else, set the width to star where star is the value returned from va_arg
// If the width is negative, set the minus flag to true and multiply the width
// by -1 to make it positive
// Finally, return 1 as true
static int	ft_set_star(char c, va_list arg, t_format *f)
{
	int	star;

	if (c != '*')
		return (0);
	if (f->dot)
	{
		star = (va_arg(arg, int));
		f->precision = star;
	}
	else
	{
		star = (va_arg(arg, int));
		if (star < 0)
		{
			f->minus = 1;
			star *= -1;
		}
		f->width = star;
	}
	return (1);
}

// Check if the character is a valid specifier
// If the character is '*' specifier, ft_set_star() will handle the format
// string and then reutrn 1 as true.
// Else if the character is a valid specifier or flag, set the value inside
// the user defined structure to 1 or the value of
// the specifier (c, s, p, x, X, d, i, u)
// Else if the character is a digit, set the width or precision to the value
// of the digit
// Else, return 0 as false
static int	ft_is_valid_specifier(char c, va_list arg, t_format *f)
{
	if (ft_set_star(c, arg, f))
		return (1);
	else if (f->dot && ft_isdigit(c))
		f->precision = f->precision * 10 + (c - '0');
	else if (ft_strchr(SPECIFIERS, c))
		f->specifier = c;
	else if (c == '-')
		f->minus = 1;
	else if (c == '0' && f->width == 0)
		f->zero = 1;
	else if (c == '.')
		f->dot = 1;
	else if (c == '#')
		f->hash = 1;
	else if (c == ' ')
		f->space = 1;
	else if (c == '+')
		f->plus = 1;
	else if (ft_isdigit(c))
		f->width = f->width * 10 + (c - '0');
	else
		return (0);
	return (1);
}

// Get the format string based on the specifier
// Set the format string to '%' as default
// Check if format is null, if it is, return NULL
// Set the index to 1 as the first character is '%'
// Reset all the values inside the user defined structure to 0
// While the string is not null and the specifier is valid,
// set the format string to the character
// Check is the string is valid, if it is, return NULL
// When specifier is set, return the format string
// Else, free the format string and return NULL
char	*ft_get_format(const char *str, va_list arg, t_format *f)
{
	int		i;
	char	*format;

	format = ft_set_format(NULL, '%', 0);
	MALLOC_NULL_CHECK(format);
	i = 1;
	ft_reset(f);
	while (str[i] && ft_is_valid_specifier(str[i], arg, f))
	{
		format = ft_set_format(format, str[i], i);
		MALLOC_NULL_CHECK(str);
		if (f->specifier)
			return (format);
		i++;
	}
	free(format);
	return (NULL);
}

// Parse the format string and return the string based on the specifier
// Specifier can be c, s, p, x, X, d, i, u, %
// If not, return an empty string using ft_calloc() with size of 1
// Check if the string is null, if it is, return NULL
// Finally, return the string
char	*ft_parse(va_list arg, t_format *f)
{
	char	*str;
	char	specifier;

	specifier = f->specifier;
	if (specifier == 'c')
		str = ft_specifier_c(va_arg(arg, int));
	else if (specifier == 's')
		str = ft_specifier_s(va_arg(arg, char *));
	else if (specifier == 'p')
		str = ft_specifier_p((unsigned long long)va_arg(arg, void *));
	else if (specifier == 'd' || specifier == 'i')
		str = ft_specifier_d_i(va_arg(arg, int));
	else if (specifier == 'u')
		str = ft_specifier_u(va_arg(arg, int));
	else if (specifier == 'x')
		str = ft_specifier_x(va_arg(arg, unsigned int), 0);
	else if (specifier == 'X')
		str = ft_specifier_x(va_arg(arg, unsigned int), 1);
	else if (specifier == '%')
		str = ft_specifier_c('%');
	else
		str = ft_calloc(sizeof(char), 1);
	MALLOC_NULL_CHECK(str);
	return (str);
}
