/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/14 22:17:45 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/16 14:25:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include <stddef.h>
# include <stdarg.h>
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif
# define MALLOC_NULL_CHECK(str) if (!str) return NULL
# define INT_MIN -2147483648
# define SPECIFIERS	"cspdiuxX%"
# define HEX_LOWER "0123456789abcdef"
# define HEX_UP "0123456789ABCDEF"
# define S_EMPTY "(null)"
# define S_EMPTY_LEN 6
# ifdef __linux__
#  define IS_LINUX 1
#  define P_EMPTY "(nil)"
#  define P_EMPTY_LEN 5
# else
#  define IS_LINUX 0
#  define P_EMPTY "0x0"
#  define P_EMPTY_LEN 3
# endif

/* ft_printf */
typedef struct s_format
{
	int		minus;
	int		plus;
	int		space;
	int		zero;
	int		hash;
	int		dot;
	int		width;
	int		precision;
	char	specifier;
}			t_format;

char		*ft_specifier_c(char c);
char		*ft_specifier_d_i(int nbr);
char		*ft_specifier_p(size_t str);
char		*ft_specifier_s(char *str);
char		*ft_specifier_u(unsigned int nbr);
char		*ft_specifier_x(unsigned int nbr, int is_upper);
char		*ft_flag(char *str, t_format *f);
char		*ft_precision(char *str, t_format *f);
char		*ft_width(char *str, t_format *f);
int			ft_len(const char *str);
int			ft_putnchar_fd(char c, int fd, int n);
int			ft_print_char(char *str, int n);
int			ft_print_str(char *str, t_format *f);
int			ft_print_empty(char *str, t_format *f);
int			ft_baselen(size_t nbr);
char		*ft_uitoa(unsigned int n);
char		*ft_itoh(size_t nbr, int is_upper, char *buffer);
void		ft_reset(t_format *f);
char		*ft_get_format(const char *str, va_list arg, t_format *f);
char		*ft_parse(va_list arg, t_format *f);
char		*ft_set_format(char *str, char c, int i);
char		*ft_strrev(char *str);
void		*ft_realloc(void *ptr, size_t size);
int			ft_printf(const char *str, ...);

/* get_next_line */
size_t		ft_strlen(const char *str);
void		ft_bzero(void *c, size_t n);
void		*ft_cleanup(void *var, void *ret);
char		*ft_get_current_line(char *buf);
char		*ft_remove_current_line(char *buf);
char		*ft_read(int fd, char *staticbuffer);
char		*get_next_line(int fd);

/* libft */
int			ft_atoi(const char *s);
void		ft_bzero(void *c, size_t n);
void		*ft_calloc(size_t nitems, size_t size);
void		*ft_memset(void *str, int c, size_t n);
int			ft_isalnum(int c);
int			ft_isalpha(int c);
int			ft_isascii(int c);
int			ft_isdigit(int c);
int			ft_isprint(int c);
char		*ft_itoa(int nb);
void		*ft_memchr(const void *str, int c, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
void		*ft_memcpy(void *dest, const void *src, size_t n);
void		*ft_memmove(void *dest, const void *src, size_t n);
void		*ft_memset(void *str, int c, size_t n);
void		ft_putchar_fd(char c, int fd);
void		ft_putendl_fd(char *s, int fd);
void		ft_putnbr_fd(int n, int fd);
void		ft_putstr_fd(char *s, int fd);
char		**ft_split(char const *s, char c);
char		*ft_strchr(const char *str, int c);
char		*ft_strdup(const char *str);
void		ft_striteri(char *s, void (*f)(unsigned int, char*));
char		*ft_strjoin(char const *s1, char const *s2);
size_t		ft_strlcat(char *dst, const char *src, size_t size);
size_t		ft_strlcpy(char *dst, const char *src, size_t dstsize);
size_t		ft_strlen(const char *str);
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int			ft_strncmp(const char *s1, const char *s2, size_t n);
char		*ft_strnstr(const char *s1, const char *s2, size_t n);
char		*ft_strrchr(const char *s, int c);
char		*ft_strtrim(char const *s1, char const *set);
char		*ft_substr(char const *s, unsigned int start, size_t len);
int			ft_tolower(int c);
int			ft_toupper(int c);
typedef struct s_list
{
	void			*content;
	struct s_list	*next;
}	t_list;
t_list		*ft_lstnew(void *content);
void		ft_lstadd_front(t_list **lst, t_list *new);
void		ft_lstadd_back(t_list **lst, t_list *new);
int			ft_lstsize(t_list *lst);
t_list		*ft_lstlast(t_list *lst);
void		ft_lstdelone(t_list *lst, void (*del)(void *));
void		ft_lstclear(t_list **lst, void (*del)(void *));
void		ft_lstiter(t_list *lst, void (*f)(void *));
t_list		*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *));

#endif