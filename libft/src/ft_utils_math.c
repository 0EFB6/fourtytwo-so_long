/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_math.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/28 18:04:50 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 00:24:45 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

// Calculate the length of the number and return the length
// Uses recursion to calculate the length
static int	ft_nbrlen(unsigned int nbr)
{
	if (nbr < 10)
		return (1);
	return (1 + ft_nbrlen(nbr / 10));
}

// Calculate the length of the target hexadecimal number length based on
// the number passed in as argument and return the length using recursion
int	ft_baselen(size_t nbr)
{
	if (nbr < 16)
		return (1);
	return (1 + ft_baselen(nbr / 16));
}

// Convert the unsigned integer to string and return the string
// The length of the number passed in is calculated using ft_nbrlen and
// assigned to len
// Uses malloc to allocate memory for the string
// Check if memory allocation is successful
// Assign the null terminator to the last index of the string or else it
// will cause error
// Use a while loop to convert the number to string
// Finally, return the string
char	*ft_uitoa(unsigned int n)
{
	int		len;
	char	*str;

	len = ft_nbrlen(n);
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

// Convert number to hexadecimal and return the buffer passed in
// which its memory is allocated using malloc or calloc
// The first index of the buffer is assigned to '0' as default
// Is uppercase is true, the uppercase hexadecimal array is assigned to hex
// else the lowercase hexadecimal array is assigned to hex
// Using a while loop, the hexadecimal is assigned to the buffer
// using the index of the hexadecimal array, the number is divided by 16
// and the remainder is used to get the hexadecimal
// The buffer is reversed using ft_strrev
// Finally, return the buffer
char	*ft_itoh(size_t nbr, int is_upper, char *buffer)
{
	int			i;
	const char	*hex;

	i = 0;
	buffer[i] = '0';
	if (is_upper)
		hex = HEX_UP;
	else
		hex = HEX_LOWER;
	while (nbr > 0)
	{
		buffer[i] = hex[nbr % 16];
		nbr /= 16;
		i++;
	}
	ft_strrev(buffer);
	return (buffer);
}
