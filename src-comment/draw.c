/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/23 10:10:13 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 22:47:05 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	draw_itom(t_data *d, int j, int i);

void	draw_image(t_data *d, void *image, int j, int i)
{
	mlx_put_image_to_window(d->mlx_ptr, d->win_ptr, image, j * PIXEL_WIDTH, i
		* PIXEL_HEIGHT);
}

/*
** Print out moves in shell and also in the so_long window
*/
void	draw_moves(t_data *d)
{
	char	*str;
	int		row;
	int		column;

	str = ft_itoa(d->count);
	draw_image(d, d->wall, 2, 0);
	ft_putstr_fd("Moves: ", 1);
	ft_putstr_fd(str, 1);
	ft_putstr_fd("\n", 1);
	free(str);
	row = get_row(d) * PIXEL_HEIGHT + 7;
	if (get_column(d) % 2 != 0)
		column = (get_column(d) - 1) / 2;
	else
		column = get_column(d) / 2;
	score(d, row, column);
}

/*
** Draw the map with wall or floor first then get images for scores via the
** score_init() function
** Draw out the Pacman Logo if the window size is latrge enough
*/
void	draw(t_data *d)
{
	int	i;
	int	j;

	i = 0;
	while (d->map[i])
	{
		j = 0;
		while (d->map[i] && d->map[i][j])
		{
			if (d->map[i][j] == '1')
				draw_image(d, d->wall, j, i);
			else if (d->map[i][j] == '0')
				draw_image(d, d->floor, j, i);
			draw_itom(d, j, i);
			j++;
		}
		i++;
	}
	score_init(d);
	if (get_column(d) >= 13)
	{
		draw_image(d, d->logo, 0, get_row(d));
		draw_image(d, d->logo, get_column(d) - 4, get_row(d));
	}
}

/*
** Draw the collectible, exit and player after drawing wall or floor
** in the main draw() function
*/
static void	draw_itom(t_data *d, int j, int i)
{
	if (d->map[i][j] == 'C')
	{
		draw_image(d, d->floor, j, i);
		draw_image(d, d->collect, j, i);
	}
	else if (d->map[i][j] == 'E')
		draw_image(d, d->exit, j, i);
	else if (d->map[i][j] == 'P')
	{
		draw_image(d, d->floor, j, i);
		draw_image(d, d->player->r, j, i);
		d->player->x = j;
		d->player->y = i;
	}
}
