/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_3.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 17:22:36 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 22:43:05 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static int	check_exit_adjacent(t_data *d, int y, int x)
{
	if (d->map[y + 1][x] == 'E' || d->map[y][x + 1] == 'E'
		|| d->map[y - 1][x] == 'E' || d->map[y][x - 1] == 'E')
		return (1);
	return (0);
}

/*
** Recursive function to check whether there is a valid path for the player to
** move around and reach the exit
*/
static int	loop(t_data *d, char **map_checker, int y, int x)
{
	map_checker[y][x] = 'Y';
	if (check_exit_adjacent(d, y, x))
		return (1);
	if ((d->map[y + 1][x] == 'C' || d->map[y + 1][x] == '0')
		&& map_checker[y + 1][x] != 'Y')
		if (loop(d, map_checker, y + 1, x))
			return (1);
	if ((d->map[y][x + 1] == 'C' || d->map[y][x + 1] == '0')
		&& map_checker[y][x + 1] != 'Y')
		if (loop(d, map_checker, y, x + 1))
			return (1);
	if ((d->map[y - 1][x] == 'C' || d->map[y - 1][x] == '0')
		&& map_checker[y - 1][x] != 'Y')
		if (loop(d, map_checker, y - 1, x))
			return (1);
	if ((d->map[y][x - 1] == 'C' || d->map[y][x - 1] == '0')
		&& map_checker[y][x - 1] != 'Y')
		if (loop(d, map_checker, y, x - 1))
			return (1);
	return (0);
}

static void	get_player_position(t_data *d, int pos[2])
{
	pos[0] = 0;
	while (d->map[pos[0]])
	{
		pos[1] = 0;
		while (d->map[pos[0]][pos[1]])
		{
			if (d->map[pos[0]][pos[1]] == 'P')
				break ;
			pos[1]++;
		}
		if (d->map[pos[0]][pos[1]] == 'P')
			break ;
		pos[0]++;
	}
}

int	check_valid_path(t_data *d)
{
	int		i;
	int		ret;
	int		pos[2];
	char	**map_checker;

	i = 0;
	map_checker = ft_calloc(get_row(d) + 1, sizeof(char *));
	while (i < get_row(d))
		map_checker[i++] = ft_calloc(get_column(d) + 1, sizeof(char));
	get_player_position(d, pos);
	ret = loop(d, map_checker, pos[0], pos[1]);
	i = 0;
	while (i < get_row(d))
		free(map_checker[i++]);
	free(map_checker);
	return (ret);
}
