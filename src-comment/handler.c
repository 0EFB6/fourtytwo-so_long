/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 13:02:24 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 22:53:00 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

/*
** Free the map array
*/
void	handle_free_map(t_data *d)
{
	int	i;

	i = 0;
	while (d->map[i])
		free(d->map[i++]);
	free(d->map);
}

/*
** Handle use keypress via the mlx_hook or mlx_loop function
** Compatible for both Mac and Linux
*/
int	handle_keycode(int keycode, t_data *data)
{
	if ((keycode == 13 || keycode == 126)
		|| (IS_LINUX && (keycode == 65362 || keycode == 119)))
		u(data);
	else if ((keycode == 1 || keycode == 125)
		|| (IS_LINUX && (keycode == 65364 || keycode == 115)))
		d(data);
	else if ((keycode == 0 || keycode == 123)
		|| (IS_LINUX && (keycode == 65361 || keycode == 97)))
		l(data);
	else if ((keycode == 2 || keycode == 124)
		|| (IS_LINUX && (keycode == 65363 || keycode == 100)))
		r(data);
	else if (keycode == 53 || (IS_LINUX && keycode == 65307))
		handle_quit(data);
	return (0);
}

/*
** Handle destroy event when user close the window
** Free the map before exiting
*/
int	handle_destroy(t_data *d)
{
	handle_free_map(d);
	free(d->enemy->l);
	free(d->enemy->r);
	exit(0);
	return (0);
}

/*
** Quit when user lose the game or win the game
** Handle destroy window and free the map array
*/
void	handle_quit(t_data *d)
{
	mlx_destroy_window(d->mlx_ptr, d->win_ptr);
	handle_free_map(d);
	free(d->enemy->l);
	free(d->enemy->r);
	exit(0);
}
