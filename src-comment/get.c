/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/22 18:16:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/26 22:50:27 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

/*
** Get the number of rows from the map
*/
int	get_row(t_data *d)
{
	int	ret;

	ret = 0;
	while (d->map[ret])
		ret++;
	return (ret);
}

/*
** Get the number of columns from the map
*/
int	get_column(t_data *d)
{
	int	ret;

	ret = 0;
	while (d->map[0] && d->map[0][ret])
		ret++;
	return (ret);
}

/*
** Random generate a suitable position for the enemy to move left and right
** Enemy will not follow player movement and also not move up or down
** Set the enemy sign to 1 to indicate that the enemy is moving to the right
*/
void	get_enemy_position(t_data *d)
{
	int	i;
	int	j;

	i = get_row(d) / 2;
	while (d->map[i])
	{
		j = get_column(d) / 2;
		while (d->map[i][j])
		{
			if (d->map[i][j] == '0' && d->map[i][j + 1] == '0')
			{
				d->enemy->x = j;
				d->enemy->y = i;
				break ;
			}
			j++;
		}
		if (d->map[i][j] == '0' && d->map[i][j + 1] == '0')
			break ;
		i++;
	}
	d->enemy->sign = 1;
}
