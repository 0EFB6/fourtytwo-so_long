# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/08/15 13:31:42 by cwei-she          #+#    #+#              #
#    Updated: 2023/08/26 19:03:19 by cwei-she         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_M = so_long
NAME_LINUX = so_long_linux

FILES_M = animation		\
		  check_map_1	\
		  check_map_2	\
		  check_map_3	\
		  draw			\
		  get			\
		  handler		\
		  init			\
		  movement		\
		  parser		\
		  score			\
		  so_long

LIBFT = ./libft/libft.a
SRC_DIR = ./src/
OBJ_DIR = ./obj/
OBJ_DIR_LINUX = ./obj-linux/
LIBFT_DIR = ./libft
MLX_DIR = ./mlx
MLX_LINUX_DIR = ./mlx_linux
RM = rm -rf
CC = cc -Wall -Werror -Wextra
CLIB = -L$(LIBFT_DIR) -lft -Lmlx -lmlx
CINCLUDE = -Ilibft/src -I$(MLX_DIR)
CINCLUDE_LINUX = -Ilibft/src -I$(MLX_LINUX_DIR)
CFRAMEWORK = -framework OpenGL -framework AppKit -o $(NAME_M)
CFRAMEWORK_B = -framework OpenGL -framework AppKit -o $(NAME_B)

OBJ =  $(addprefix $(OBJ_DIR), $(addsuffix .o, $(FILES_M)))
OBJ_LINUX =  $(addprefix $(OBJ_DIR_LINUX), $(addsuffix .o, $(FILES_M)))

RESET = \033[0m
BOLD = \033[1m
DIM = \033[2m
UNDERLINE = \033[4m
BLINK = \033[5m
INVERT = \033[7m
LIGHT_BLUE = \033[94m
YELLOW = \033[93m

all:		$(NAME_M)

linux:		$(NAME_LINUX)

bonus:		all

clean:
			@$(RM) $(OBJ_DIR) $(OBJ_DIR_LINUX)
			@make --silent --no-print-directory -C libft clean

fclean:		clean
			@$(RM) $(NAME_M) $(NAME_LINUX)
			@make --silent --no-print-directory -C libft fclean

re:			fclean all

norm:
			norminette -R CheckDefine src/
			norminette -R CheckDefine libft/

$(LIBFT):
			@make --silent --no-print-directory -C libft bonus
			@echo "$(BOLD)$(LIGHT_BLUE)Libft : SUCCESS!$(RESET)"

$(NAME_M):	$(LIBFT) $(OBJ)
			@echo "$(BOLD)$(LIGHT_BLUE)Linking objects (MacOS)...$(RESET)"
			@$(CC) $(OBJ) $(CLIB) $(CFRAMEWORK)
			@echo "$(BOLD)$(LIGHT_BLUE)$(NAME_M) (mandatory & bonus): SUCCESS!$(RESET)"
			@echo "$(BOLD)Copyright Reserved © Wilson Chang$(RESET)"

$(NAME_LINUX):	$(LIBFT) $(OBJ_LINUX)
				@echo "$(BOLD)$(LIGHT_BLUE)Linking objects (Linux)...$(RESET)"
				@$(CC) $(OBJ_LINUX) -Imlx_linux -L$(LIBFT_DIR) -lft -Lmlx_linux -lmlx_Linux -L/usr/lib/x86_64-linux-gnu -lXext -lX11 -lm -lz -o $(NAME_LINUX)
				@echo "$(BOLD)$(LIGHT_BLUE)$(NAME_M) (mandatory & bonus): SUCCESS!$(RESET)"
				@echo "$(BOLD)Copyright Reserved © Wilson Chang$(RESET)"

$(OBJ): | $(OBJ_DIR)

$(OBJ_DIR):
			@mkdir -p $(OBJ_DIR)/
			@echo "$(BOLD)$(LIGHT_BLUE)Object directory (MacOS) created!$(RESET)"

$(OBJ_DIR)%.o:$(SRC_DIR)%.c
			@echo "$(BOLD)$(YELLOW)Compiling $<...$(RESET)"
			@$(CC) $(CINCLUDE) -c -o $@ $^

$(OBJ_LINUX): | $(OBJ_DIR_LINUX)

$(OBJ_DIR_LINUX):
			@mkdir -p $(OBJ_DIR_LINUX)/
			@echo "$(BOLD)$(LIGHT_BLUE)Object directory (Linux) created!$(RESET)"

$(OBJ_DIR_LINUX)%.o:$(SRC_DIR)%.c
			@echo "$(BOLD)$(YELLOW)Compiling $<...$(RESET)"
			@$(CC) $(CINCLUDE_LINUX) -c -o $@ $^

.PHONY:all bonus clean fclean re norm